const addOffer = require("./resolvers/add_offer");
const updateOffer = require("./resolvers/update_offer");
const getOffers = require("./resolvers/get_offers");
const { pubsub } = require("../../constants/pubsub");
const { withFilter } = require("graphql-subscriptions");

module.exports = {
  Subscription: {
    changedOffer: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("CHANGED_OFFER"),
        (payload, variables) => {
          // Only push an update if the comment is on
          // the correct repository for this operation
          //console.log(variables.user);
          //console.log(payload.changedBet.user._id);
          return (
            payload.changedBet
          );
        }
      ),
    },
  },
  Query: {
    getOffers,
  },
  Mutation: {
    addOffer,
    updateOffer,
  },
};
