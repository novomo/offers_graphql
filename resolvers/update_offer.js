const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");

const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

const pooling = {
  enabled: true,
  maxIdleTime: 500,
  maxSize: 20,
  queueTimeout: 500,
};

const IP = require('ip');
const upload_error = require("../../../node_error_functions/upload_error")
const { pubsub } = require("../../../constants/pubsub");
const compileBet = require("../constants/compile_bet");

module.exports = async (
  _,
  { inputOffer },
  {
    currentUser,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {

    if (!inputOffer._id) {
      throw new Error("no offer _id was given!");
    }
    let client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
      pooling,
    });
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await client.getSession();
    } catch (err) {
      await client.close();
      client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
        pooling,
      });

      mysqlClient = await client.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(MYSQL_DATABASE);
    // get get user

    
    const offer_collection = await mysqlDb.getCollection("offers");
    let offer_id = await offer_collection
      .modify(`_id = '${inputOffer}'`)
      .patch({ ...inputOffer })
      .execute();

    // compile bet and return

    let offer = await offer_collection.find(`_id = '${inputOffer}'`)

    pubsub.publish("CHANGED_OFFER", {
      changedBet: { ...offer },
    });
    return offer;
  } catch (err) {
    console.log(err)
    upload_error({
      errorTitle: "Updating offer",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
  }
};
