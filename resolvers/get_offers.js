const mysqlx = require("@mysql/xdevapi");
const IP = require('ip');
const upload_error = require("../../../node_error_functions/upload_error")
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

const pooling = {
  enabled: true,
  maxIdleTime: 500,
  maxSize: 20,
  queueTimeout: 500,
};

const compileBets = require("../constants/compile_bets");

module.exports = async (
  _,
  { query },
  {
    currentUser,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {

    let client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
      pooling,
    });
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await client.getSession();
    } catch (err) {
      await client.close();
      client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
        pooling,
      });

      mysqlClient = await client.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(MYSQL_DATABASE);
    // get get user

    const offer_collection = await mysqlDb.getCollection("offers");
    let offers = await betColl.find(query).execute();

    offers = offers.fetchAll();

    return offers;
  } catch (err) {
    console.log(err)
    upload_error({
      errorTitle: "Getting offers",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
  }
};
