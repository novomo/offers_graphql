const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");
const {
  currencyConverter,
} = require("../../../constants/currency_converter/converter");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
const IP = require('ip');
const upload_error = require("../../../node_error_functions/upload_error")
const pooling = {
  enabled: true,
  maxIdleTime: 500,
  maxSize: 20,
  queueTimeout: 500,
};
const { pubsub } = require("../../../constants/pubsub");
const compileBet = require("../constants/compile_bet");

module.exports = async (
  _,
  { InputOffer },
  {
    currentUser,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    console.log(inputBet);
    let client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
      pooling,
    });
    let mysqlClient = null;
    let mysqlDb = null;
    try {
      mysqlClient = await client.getSession();
    } catch (err) {
      await client.close();
      client = mysqlx.getClient(`${MYSQL_USER}:${MYSQL_PASS}@${MYSQL_HOST}`, {
        pooling,
      });

      mysqlClient = await client.getSession();
    }
    mysqlDb = await mysqlClient.getSchema(MYSQL_DATABASE);
    // get get user

    const offer_collection = await mysqlDb.getCollection("offers");

    let offer_id = await offer_collection
      .add({
        ...InputOffer,
      })
      .execute();
      offer_id = offer_id.getGeneratedIds()[0];
    // compile bet and return

    let offer = await offer_collection.find(`_id = '${offer_id};`)

    pubsub.publish("CHANGED_OFFER", {
      changedOffer: { ...offer },
    });
    return offer;
  } catch(err) {
    console.log(err)
    upload_error({
      errorTitle: "Adding offer",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })
}
};
